/*
    Motors4SoccerRobot.cpp - Library to control 4 motors of a soccer robot
    which are connected to a L298N simultaneously with advanced functions.
    Created by Tobias Veselsky, January 1, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#include "Arduino.h"
#include "Motors4SoccerRobot.h"

const static float pi = 3.14159267;

Motors4SoccerRobot::Motors4SoccerRobot(L298NMotorControl *motor1, L298NMotorControl *motor2, L298NMotorControl *motor3, L298NMotorControl *motor4) {
    /* Constructor */

    _motor1 = motor1;           //initialise private variables
    _motor2 = motor2;
    _motor3 = motor3;
    _motor4 = motor4;
    _areMotorsSpinning = false;
    _rotationSpeed = 0;
    _direction = 0;
    _driveSpeed = 0;
}

boolean Motors4SoccerRobot::areMotorsSpinning() {
    /* this function returns true if at least one of the 4 motors is supposed to be spinning and false if all motors are not supposed to be spinning */

    //TODO: Maybe fix needed???
    return _areMotorsSpinning;
}

void Motors4SoccerRobot::stopMotors() {
    /* this function calls the stop function of all 4 motors, so that they actively reduce their speed to a stop */

    _motor1->stop();
    _motor2->stop();
    _motor3->stop();
    _motor4->stop();
}

int16_t Motors4SoccerRobot::getRotationSpeed() {
    /* this function returns the current set rotation speed value (range: -255 to 255) */

    return _rotationSpeed;
}

void Motors4SoccerRobot::setRotationSpeed(int16_t rotationSpeed) {
    /* this function is used to set the rotation speed (z-Rotation) of the robot (parameter range: -255 to 255). Positive values stand for a clockwise rotation and negative values will make the robot turn counterclockwise. */

    _rotationSpeed = rotationSpeed;
}

void Motors4SoccerRobot::setDirectionAndSpeed(int16_t direction, uint8_t driveSpeed) {
    /* this function is used to let the robot drive at a given speed into a given direction in degrees */

    _direction = direction;
    _driveSpeed = driveSpeed;
}

void Motors4SoccerRobot::applySpeeds() {
    /* the different setSpeed functions are called within one program loop and are used to set all the speed variables.
    After setting all the speeds, you can simply call this function to use all these variables to calculate the corresponding speed for every single motor and apply these speeds to those motors automatically. */

    int32_t motorSpeeds[4];

    int winkel0 = 135 - _direction;
    int winkel1 = 225 - _direction;
    int winkel2 = 315 - _direction;
    int winkel3 = 45 - _direction;
    motorSpeeds[0] = _rotationSpeed - _driveSpeed * sin(winkel0 / 360.0f * 2.0f * pi);
    motorSpeeds[1] = _rotationSpeed - _driveSpeed * sin(winkel1 / 360.0f * 2.0f * pi);
    motorSpeeds[2] = _rotationSpeed - _driveSpeed * sin(winkel2 / 360.0f * 2.0f * pi);
    motorSpeeds[3] = _rotationSpeed - _driveSpeed * sin(winkel3 / 360.0f * 2.0f * pi);

    limitSpeedToMaximum(motorSpeeds);

    _motor1->smartDrive(motorSpeeds[0]);
    _motor2->smartDrive(motorSpeeds[1]);
    _motor3->smartDrive(motorSpeeds[2]);
    _motor4->smartDrive(motorSpeeds[3]);
}

void Motors4SoccerRobot::limitSpeedToMaximum(int32_t *motorSpeeds) {
    /* takes the different motor speeds as an argument and then calculates the maximum speed for every single motor while still keeping the proportion of the different speeds and therefore for example not changing the direction.
    The function then overrides the integer array which holds the speeds of all 4 motors */

    //TODO: Correct implementation still missing
    if (motorSpeeds[0]>255) {
        motorSpeeds[0] = 255;
    } else if (motorSpeeds[0]<-255) {
        motorSpeeds[0] = -255;
    }
    if (motorSpeeds[1]>255) {
        motorSpeeds[1] = 255;
    } else if (motorSpeeds[1]<-255) {
        motorSpeeds[1] = -255;
    }
    if (motorSpeeds[2]>255) {
        motorSpeeds[2] = 255;
    } else if (motorSpeeds[2]<-255) {
        motorSpeeds[2] = -255;
    }
    if (motorSpeeds[3]>255) {
        motorSpeeds[3] = 255;
    } else if (motorSpeeds[3]<-255) {
        motorSpeeds[3] = -255;
    }
    

    if (motorSpeeds[0] != 0) motorSpeeds[0] = (motorSpeeds[0]>0) ? map(motorSpeeds[0], 1, 255, 30, 255) : map(motorSpeeds[0], -1, -255, -30, -255);
    if (motorSpeeds[1] != 0) motorSpeeds[1] = (motorSpeeds[1]>0) ? map(motorSpeeds[1], 1, 255, 30, 255) : map(motorSpeeds[1], -1, -255, -30, -255);
    if (motorSpeeds[2] != 0) motorSpeeds[2] = (motorSpeeds[2]>0) ? map(motorSpeeds[2], 1, 255, 30, 255) : map(motorSpeeds[2], -1, -255, -30, -255);
    if (motorSpeeds[3] != 0) motorSpeeds[3] = (motorSpeeds[3]>0) ? map(motorSpeeds[3], 1, 255, 30, 255) : map(motorSpeeds[3], -1, -255, -30, -255);
}