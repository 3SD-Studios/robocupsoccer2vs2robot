/*
    Motors4SoccerRobot.h - Library to control 4 motors of a soccer robot
    which are connected to a L298N simultaneously with advanced functions.
    Created by Tobias Veselsky, January 1, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#ifndef Motors4SoccerRobot_h
#define Motors4SoccerRobot_h

#include "Arduino.h"
#include "L298NMotorControl.h"

class Motors4SoccerRobot {
    public:
        Motors4SoccerRobot(L298NMotorControl *motor1, L298NMotorControl *motor2, L298NMotorControl *motor3, L298NMotorControl *motor4);
        boolean areMotorsSpinning();
        void stopMotors();
        int16_t getRotationSpeed();
        void setRotationSpeed(int16_t rotationSpeed);
        void setDirectionAndSpeed(int16_t direction, uint8_t driveSpeed);
        void applySpeeds();
    private:
        L298NMotorControl *_motor1;
        L298NMotorControl *_motor2;
        L298NMotorControl *_motor3;
        L298NMotorControl *_motor4;
        boolean _areMotorsSpinning;
        int16_t _rotationSpeed;
        uint8_t _driveSpeed;
        int16_t _direction;
        void limitSpeedToMaximum(int32_t *motorSpeeds);
};

#endif