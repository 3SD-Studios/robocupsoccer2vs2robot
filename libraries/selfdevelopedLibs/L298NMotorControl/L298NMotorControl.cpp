/*
    L298NMotorControl.cpp - Library to control a single motor connected to a L298N.
    This library is ment to be used with with the Motor4SoccerRobot library but can also be used as a standalone library.
    Created by Tobias Veselsky, January 1, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#include "Arduino.h"
#include "L298NMotorControl.h"

L298NMotorControl::L298NMotorControl(uint8_t pinMotorIN1, uint8_t pinMotorIN2, uint8_t pinMotorSPEED) {
    /* constructor */

    _pinMotorIN1 = pinMotorIN1;     //set objects private variables
    _pinMotorIN2 = pinMotorIN2;
    _pinMotorSPEED = pinMotorSPEED;
    _motorIsSpinning = false;
    _speed = 0;

    pinMode(_pinMotorIN1, OUTPUT);  //set pins as OUTPUT
    pinMode(_pinMotorIN2, OUTPUT);
    pinMode(_pinMotorSPEED, OUTPUT);
}

uint8_t L298NMotorControl::getSpeed() {
    /* returns the current set speed (8 bit value -> 0-255) */
    
    return _speed;
}

void L298NMotorControl::setSpeed(uint8_t speed) {
    /* set the motor speed (8bit value -> 0-255)
    !!! important: this function does only change the variables value, but does not automatically apply the new speed to the motor!
    Therefore you have to call forward() or backward() to apply the new speed to the actual motor!!! */

    _speed = speed;
}

void L298NMotorControl::forward() {
    /* by calling this function, the motor will spin forward with the speed set by the setSpeed() function */

    _motorIsSpinning = (_speed > 0) ? true : false;        //set _motorIsSpinning to true if the motor's speed is greater than 0, else set it to false

    digitalWrite(_pinMotorIN1, HIGH);   //set IN pins differently to set one of the 2 spinning directions. Since forward and backward directions do depend on the motors connection, I've defined HIGH, LOW as forward :)
    digitalWrite(_pinMotorIN2, LOW);
    analogWrite(_pinMotorSPEED, _speed);    //set the motors speed via the Arduinos PWM modulation
}

void L298NMotorControl::backward() {
    /* by calling this function, the motor will spin backward with the speed set by the setSpeed() function. The implementation is pretty much the same as in the forward function*/

    _motorIsSpinning = (_speed > 0) ? true : false;        //set _motorIsSpinning to true if the motor's speed is greater than 0, else set it to false

    digitalWrite(_pinMotorIN1, LOW);   //set IN pins differently to set one of the 2 spinning directions. Here I've set IN1 and IN2 to the exact opposite as in the forward() function to let the motor spin in the opposite direction :)
    digitalWrite(_pinMotorIN2, HIGH);
    analogWrite(_pinMotorSPEED, _speed);    //set the motors speed via the Arduinos PWM modulation
}

void L298NMotorControl::setDirection(uint8_t direction) {
    /* this function can be called with the direction as a parameter and according to the given direction value it then simply calls either the forward() or the backward() function */

    switch (direction) {
        case FORWARD:
            this->forward();
            break;
        case BACKWARD:
            this->backward();
            break;
    }
}

void L298NMotorControl::smartDrive(int16_t speed) {
    /* this function can become very handy when working with several motors and PID functions etc.
    It simply just needs a speed variable as a parameter and is the only function to be called to set the motor speed and direction simultaneouly.
    Just set the speed to a value between (-255-255) and the function will automatically call the setSpeed() function and according to the given speed value also call the forward() or backward() function.
    positive speed -> forward           |           negative speed -> backward */

    _speed = abs(speed);        //set the objects speed variable (always positive)

    if (speed >= 0) {           //positive speed -> forward (intentionally including 0 so that the motors speed can be set to 0)
        forward();
    }
    else {                      //if the speed variable was negative let the motor spin backwards
        backward();
    }
}

void L298NMotorControl::stop() {
    /* by calling this function, the motor will not be cut from power, but actively slowed down (motor force stop). */

    _speed = 0;     //set the speed variable's value to 0. This is not needed, but I think it just makes more sense like that.
    _motorIsSpinning = false;

    digitalWrite(_pinMotorIN1, LOW);    //set both IN pins to LOW
    digitalWrite(_pinMotorIN2, LOW);
    analogWrite(_pinMotorSPEED, 255);   //by setting the speed to max and both IN pins to LOW, the motor will be actively slowed down.
}

boolean L298NMotorControl::isMotorSpinning() {
    /* this function just returns a boolean which equals true if the motor is currently set to spin and false if it is set to be stopped */

    return _motorIsSpinning;
}