/*
    L298NMotorControl.h - Library to control a single motor connected to a L298N.
    This library is ment to be used with with the Motor4SoccerRobot library but can also be used as a standalone library.
    Created by Tobias Veselsky, January 1, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#ifndef L298NMotorControl_h
#define L298NMotorControl_h

#include "Arduino.h"

class L298NMotorControl {
    public:
        L298NMotorControl(uint8_t pinMotorIN1, uint8_t pinMotorIN2, uint8_t pinMotorSPEED);

        enum _direction {
            FORWARD = 0,
            BACKWARD = 1
        };
        typedef enum _direction direction;

        uint8_t getSpeed();
        void setSpeed(uint8_t speed);
        void forward();
        void backward();
        void setDirection(uint8_t direction);
        void smartDrive(int16_t speed);
        void stop();
        boolean isMotorSpinning();

    private:
        uint8_t _pinMotorIN1;
        uint8_t _pinMotorIN2;
        uint8_t _pinMotorSPEED;
        uint8_t _speed;
        boolean _motorIsSpinning;
};

#endif