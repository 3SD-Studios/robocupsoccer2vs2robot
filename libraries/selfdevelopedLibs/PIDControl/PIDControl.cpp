/*
    PIDControl.h - PID control implementation as an Arduino library
    this library is particularly designed for a soccer robot, but can be generaly
    used in every project as a PID implementation.
    Created by Tobias Veselsky, January 10, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#include "Arduino.h"
#include "PIDControl.h"

PIDControl::PIDControl(double pP, double pI, double pD) {
    /* Contructor */

    _input = 0;     //initialise objects variables
    _output = 0;
    _setpoint = 0;
    _pidOutput_I = 0;
    _lastError_D = 0;
    _pP = pP;
    _pI = pI;
    _pD = pD;

    _sampleRate = 100;              //set default sample rate to 100ms
    _lastCalculationMillis = 0;     //set _lastCalculationMillis to 0 to let the first call of the calculate function trigger an actual calculation :)
    _maxOutputAbsolute = 255;       //set _maxOutputAbsolute as default to 255 to limit the PIDControls output to the range [-255...255].
}

boolean PIDControl::calculate() {
    /* call this function, to trigger a calculation of the PID output. The output is being written to the set *_output address. If the rate this function is being called is higher than the sample rate, then calculations are only made at the sampleRate's "speed". The function returns true if a new calcualtion was made, and false if not. */

    long currentMillis = millis();      //save the current millis timer value
    if (currentMillis - _lastCalculationMillis <= _sampleRate) return false;        //the time between 2 calculations must be at least sampleRate's millis (to get a more stable result). Return false if no new output was calculated.


    double pidError = _input - _setpoint;       //calculate error variable

    //calculate and limit the integral error :)
    _pidOutput_I+= _pI * pidError;        //integrate the error value
    if (_pidOutput_I > _maxOutputAbsolute) _pidOutput_I = _maxOutputAbsolute;       //limit the pidOutput_I      
    else if (_pidOutput_I < -_maxOutputAbsolute) _pidOutput_I = -_maxOutputAbsolute;



    //calculate the output value
    _output = _pP * pidError + _pidOutput_I + _pD * (pidError - _lastError_D);         //calculate the pid output
    if (_output > _maxOutputAbsolute) _output = _maxOutputAbsolute;                 //limit the output value      
    else if (_output < -_maxOutputAbsolute) _output = -_maxOutputAbsolute;


    _lastError_D = pidError;                    //set the _lastError_D value to the current error
    _lastCalculationMillis = currentMillis;     //set _lastCalculationMillis to the time when the calculation started
    return true;    //return true if a new output was being calculated
}

void PIDControl::setPIDParameters(double pP, double pI, double pD) {
    /* this function simply overrides the PID paramters and can for example be used to tune the parameters or to set these dynamically at runtime :) */
    
    _pP = pP;
    _pI = pI;
    _pD = pD;
}

void PIDControl::setSampleRate(int sampleRate) {
    /* this function must be called to change the default set sampleRate to a different value. The parameter is the minimum time between two calculations in milliseconds */

    _sampleRate = sampleRate;
}

double PIDControl::getParameterP() {
    /* this function simply returns the current set proportional gain value */

    return _pP;
}

double PIDControl::getParameterI() {
    /* this function simply returns the current set integral gain value */

    return _pI;
}

double PIDControl::getParameterD() {
    /* this function simply returns the current set derivative gain value */

    return _pD;
}

void PIDControl::setSetpoint(double setpoint) {
    /* simply set the PID setpoint */

    _setpoint = setpoint;
}

double PIDControl::getSetpoint() {
    /* simply return the current set setpoint value */

    return _setpoint;
}

void PIDControl::setInput(double input) {
    /* simply set the PID input value */

    _input = input;
}

double PIDControl::getInput() {
    /* simply return the current input value */

    return _input;
}

double PIDControl::getOutput() {
    /* simply return the current output value */

    return _output;
}

void PIDControl::setMaxOutputAbsolute(double maxOutputAbsolute) {
    /* simply set the PID max output absolute value */

    _maxOutputAbsolute = maxOutputAbsolute;
}

double PIDControl::getMaxOutputAbsolute() {
    /* simply return the current maxOutputAbsolute value */

    return _maxOutputAbsolute;
}

void PIDControl::resetOldValues() {
    /* you can call this function if you want to reset values of previous calculations */

    _pidOutput_I = 0;
    _lastError_D = 0;
    _output = 0;
}