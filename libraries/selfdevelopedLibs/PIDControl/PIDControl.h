/*
    PIDControl.h - PID control implementation as an Arduino library
    this library is particularly designed for a soccer robot, but can be generaly
    used in every project as a PID implementation.
    Created by Tobias Veselsky, January 10, 2020.
    All rights reserved. Copyright Tobias Veselsky 2020.
*/

#ifndef PIDControl_h
#define PIDControl_h

#include "Arduino.h"


class PIDControl {
    public:
        PIDControl(double pP, double pI, double pD);
        boolean calculate();
        void setPIDParameters(double pP, double pI, double pD);
        void setSampleRate(int sampleRate);
        double getParameterP();
        double getParameterI();
        double getParameterD();
        void setSetpoint(double setpoint);
        double getSetpoint();
        void setInput(double input);
        double getInput();
        double getOutput();
        void setMaxOutputAbsolute(double maxOutputAbsolute);
        double getMaxOutputAbsolute();
        void resetOldValues();
    private:
        double _input;
        double _output;
        double _pP;
        double _pI;
        double _pD;
        int _sampleRate;
        long _lastCalculationMillis;
        double _setpoint;
        double _maxOutputAbsolute;
        double _pidOutput_I;
        double _lastError_D;
};



#endif