# RoboCupSoccer2vs2Robot

This is the source-code for our robots competing in at Robocup German Open 2020. It was written by me in a few month before the actual matches. Eventhough we had barely enough time and some important things were implemented just shorly before the matches, we still managed to win every single game in Berlin, Germany and became first place.
We therefore qualified for the nationwide RoboCup competition, but due to Corona that is still in the future.

# Main features:

  - Own libraries to control motors via L298N, calculate motor-speeds with different given direction inputs and a library for PID-Controller inplementation
  - Basic position determination
  - Compass based orientation
  - Camera based detection and follow of an orange soccerball
  - Ability for Bluetooth communication between the robots
  - Detection of white lines of the matchfield


# Known Bugs:
  - The used BNO055 has a known issue where, if the robot hits an object like one of the goals quite hard, the integrated gyro or accelerometer would mess up all the rotational data. In some of our games this actually happened and one of our robots tried to fight the other. This can easily be fixed and will be fixed in the future.


# License:
Copyright Tobias Veselsky - All Rights Reserved  
Unauthorized copying of this file or any file contained in this repository, via any medium is strictly prohibited  
Proprietary and confidential  
Usage of this or any file in this repository has to be authored by Tobias Veselsky (3sd.studios@gmail.com)  
Copyright to any authorized derivative of this project still belongs to Tobias Veselsky (3SD-Studios)  
Unauthorized derivatives are prohibited  
Written by Tobias Veselsky (3SD-Studios) <3sd.studios@gmail.com>, published May 2020  