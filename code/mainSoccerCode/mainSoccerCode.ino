/* Copyright Tobias Veselsky - All Rights Reserved
 * Unauthorized copying of this file or any file contained in this repository, via any medium is strictly prohibited
 * Proprietary and confidential
 * Usage of this or any file in this repository has to be authored by Tobias Veselsky (3sd.studios@gmail.com)
 * Copyright to any authorized derivative of this project still belongs to Tobias Veselsky (3SD-Studios)
 * Unauthorized derivatives are prohibited
 * Written by Tobias Veselsky (3SD-Studios) <3sd.studios@gmail.com>, published May 2020
 */


#include <Adafruit_BNO055.h>
#include "FastLED.h"
#include "L298NMotorControl.h"
#include "Motors4SoccerRobot.h"
#include "PIDControl.h"
#include <Pixy2.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

/**
 * motor-pinmapping: IN1 and IN2 are 2 inputs pins of the respective L6205N's.
 * The direction of rotation can be specified via these 2 pins.
 * By setting both to true or both to false, the motor stops spinning.
 * The SPEED pin is used to set the motor speed via pulse width modulation.
 **/
#define MOTOR1_IN1 40
#define MOTOR1_IN2 38
#define MOTOR1_SPEED 5
#define MOTOR2_IN1 44
#define MOTOR2_IN2 42
#define MOTOR2_SPEED 4
#define MOTOR3_IN1 46
#define MOTOR3_IN2 48
#define MOTOR3_SPEED 3
#define MOTOR4_IN1 47
#define MOTOR4_IN2 49
#define MOTOR4_SPEED 2

/* pushbutton-pinmapping */
#define BUTTON_1 41
#define BUTTON_2 39
#define BUTTON_3 28
#define BUTTON_4 29

/* poti-pinmapping */
#define POTI_1 A11
#define POTI_2 A12

/* led-pinmapping */
#define LED_1 24
#define LED_2 23
#define LED_3 25
#define LED_4 22

/* light-barrier-pinmapping */
#define LBARRIER_TX 37
#define LBARRIER_RX A13

/* buzzer */
#define BUZZER_PIN 31

/* kicker */
#define KICKER_PIN 26

/* WS2812B */
#define WS2812B_ANZAHL_LEDS 33
#define WS2812B_PIN 45

/* I2C Addresses */
#define BNO055_I2C_ADDRESS 55

//these are internal INT codes sent by one of our other UCs specifying which soccerfield line was crossed by the robot
#define LINE_NONE 0
#define LINE_FRONT 1
#define LINE_RIGHT 3
#define LINE_BACK 4
#define LINE_LEFT 2

Adafruit_BNO055 bno055 = Adafruit_BNO055(BNO055_I2C_ADDRESS);

CRGB ws2812b_leds[WS2812B_ANZAHL_LEDS];

L298NMotorControl motor1 = L298NMotorControl(MOTOR1_IN1, MOTOR1_IN2, MOTOR1_SPEED);
L298NMotorControl motor2 = L298NMotorControl(MOTOR2_IN1, MOTOR2_IN2, MOTOR2_SPEED);
L298NMotorControl motor3 = L298NMotorControl(MOTOR3_IN1, MOTOR3_IN2, MOTOR3_SPEED);
L298NMotorControl motor4 = L298NMotorControl(MOTOR4_IN1, MOTOR4_IN2, MOTOR4_SPEED);

Motors4SoccerRobot motors4SR = Motors4SoccerRobot(&motor1, &motor2, &motor3, &motor4);

PIDControl compassPIDControl = PIDControl(0.9, 0.0, 1.5);
PIDControl followBallXPIDControl = PIDControl(1.2, 0.0, 1.0);
PIDControl followBallYPIDControl = PIDControl(1.6, 0.0, 1.0);
PIDControl moveBallToGoalX = PIDControl(0.5, 0.0, 0.7);
PIDControl moveBallToGoalY = PIDControl(0.5, 0.0, 0.7);

Pixy2 pixy;

int hsv = 0;

LiquidCrystal_I2C lcd(0x27,16,2);

volatile byte stateUsS0, stateUsS1, stateUsS2, stateUsS3, stateUsS4;
volatile unsigned long timerUsS0, timerUsS1, timerUsS2, timerUsS3, timerUsS4;
int gotUsSReadout[8];
unsigned long rawInputUsS[8]; 
float ultrasonicDistanceValuesMM[8];
int currentUsSensor;
unsigned long currentUsSensorTimeTriggered;
uint8_t detectedLine = 0;

void setup()
{
    Serial.begin(9600);                                         //Establish a Serial Connection with a Baud rate of 9600
    Wire.begin();
    Serial.println("Starting MainSoccerRobot 2020 code\n");

    FastLED.addLeds<NEOPIXEL, WS2812B_PIN>(ws2812b_leds, WS2812B_ANZAHL_LEDS);

    bno055.begin();
    bno055.setExtCrystalUse(true);                              //it is recommended to use the external crystal for better accuracy

    pixy.init();

    compassPIDControl.setMaxOutputAbsolute(150);
    compassPIDControl.setSampleRate(50);
    followBallXPIDControl.setSampleRate(50);
    followBallYPIDControl.setSampleRate(50);
    moveBallToGoalX.setSampleRate(50);
    moveBallToGoalY.setSampleRate(50);

    playBuzzerFeedbackSound();
    //pixy.setLamp(1, 1);     //can be enabled to turn on the PIXYs LEDs as a white lamp with max brightness

    pinMode(LBARRIER_TX, OUTPUT);
    digitalWrite(LBARRIER_TX, HIGH);


    PCICR |= (1 << PCIE1);                                       //Set PCIE1 to enable PCMSK1 scan.
    PCICR |= (1 << PCIE2);                                       //Set PCIE2 to enable PCMSK2 scan.
    PCMSK1 |= (1 << PCINT9);                                     //Set PCINT9 (digital input 15) to trigger an interrupt on state change.
    PCMSK1 |= (1 << PCINT10);                                    //Set PCINT10 (digital input 14) to trigger an interrupt on state change.
    PCMSK2 |= (1 << PCINT17);                                    //Set PCINT17 (analog input 9) to trigger an interrupt on state change.
    PCMSK2 |= (1 << PCINT18);                                    //Set PCINT18 (analog input 10) to trigger an interrupt on state change.
    pinMode(10, OUTPUT);
    pinMode(11, OUTPUT);
    pinMode(12, OUTPUT);
    pinMode(13, OUTPUT);

    lcd.init();
    lcd.backlight();
}

void loop()
{
    lcd.clear();

    sensors_event_t event; 
    bno055.getEvent(&event);

    double simplediff = event.orientation.x;
    simplediff = (simplediff > 180) ? simplediff - 360 : simplediff;

    compassPIDControl.setInput(simplediff);
    compassPIDControl.calculate();

    pixy.ccc.getBlocks();

    int xOffset = 0;
    int ballSize = -1;
    if (pixy.ccc.numBlocks) {
        xOffset = (pixy.frameWidth - pixy.ccc.blocks[0].m_x) - pixy.frameWidth/2;
        ballSize = pixy.ccc.blocks[0].m_y;
    }

    motors4SR.setRotationSpeed((int) compassPIDControl.getOutput());

    if (analogRead(POTI_1) >= 15) {
      switch (detectedLine) {
        case LINE_FRONT:
          motors4SR.setDirectionAndSpeed(270, 255);
          motors4SR.setRotationSpeed(0);
          motors4SR.applySpeeds();
          delay(100);
          break;
        case LINE_LEFT:
          motors4SR.setDirectionAndSpeed(0, 255);
          motors4SR.setRotationSpeed(0);
          motors4SR.applySpeeds();
          delay(100);
          break;
        case LINE_BACK:
          motors4SR.setDirectionAndSpeed(90, 255);
          motors4SR.setRotationSpeed(0);
          motors4SR.applySpeeds();
          delay(100);
          break;
        case LINE_RIGHT:
          motors4SR.setDirectionAndSpeed(180, 255);
          motors4SR.setRotationSpeed(0);
          motors4SR.applySpeeds();
          delay(100);
          break;
      }
    }

    digitalWrite(LED_1, (detectedLine));


    float fieldPosX = ultrasonicDistanceValuesMM[1] / (ultrasonicDistanceValuesMM[1] + ultrasonicDistanceValuesMM[3]);
    float fieldPosY = ultrasonicDistanceValuesMM[0] / (ultrasonicDistanceValuesMM[0] + ultrasonicDistanceValuesMM[2]);


    followBallXPIDControl.setInput(xOffset);
    followBallXPIDControl.calculate();
    followBallYPIDControl.setInput(ballSize);
    followBallYPIDControl.calculate();
    
    moveBallToGoalX.setInput(ultrasonicDistanceValuesMM[1]-ultrasonicDistanceValuesMM[3]);
    moveBallToGoalX.calculate();
    moveBallToGoalY.setInput(ultrasonicDistanceValuesMM[0]);
    moveBallToGoalY.calculate();
    int speedResultFollowBall = sqrt(followBallXPIDControl.getOutput() * followBallXPIDControl.getOutput() + followBallYPIDControl.getOutput() * followBallYPIDControl.getOutput());
    int winkelFollowBall = round(atan2(followBallYPIDControl.getOutput(), followBallXPIDControl.getOutput()) * 180 / 3.14159265359);

    speedResultFollowBall = (speedResultFollowBall <= 255) ? speedResultFollowBall : 255;
    
    motors4SR.setDirectionAndSpeed(winkelFollowBall, speedResultFollowBall);
    /*
    if (analogRead(LBARRIER_RX) < 15) {
      int speed = sqrt(moveBallToGoalX.getOutput() * moveBallToGoalX.getOutput() + moveBallToGoalY.getOutput() * moveBallToGoalY.getOutput());
      speed = (speed <= 255) ? speed : 255;
      int winkel = round(atan2(moveBallToGoalY.getOutput(), moveBallToGoalX.getOutput()) * 180 / 3.14159265359);
      Serial.println(String(winkel)+"..."+String(speed)+"..."+String(moveBallToGoalX.getOutput())+"..."+String(moveBallToGoalY.getOutput()));
      motors4SR.setDirectionAndSpeed(winkel, speed);
      lcd.setCursor(0, 1);
      lcd.print(String(analogRead(LBARRIER_RX)) + "___BALL___" + String(moveBallToGoalX.getOutput()));
      digitalWrite(LED_2, HIGH);
    }
    */
    if(ballSize==-1) {
      motors4SR.setDirectionAndSpeed(270, 100);
    }

    if(analogRead(POTI_1) < 5) {
        motors4SR.setDirectionAndSpeed(0, 0);
        motors4SR.setRotationSpeed(0);
        hsv = (hsv<255) ? hsv+1 : 0;
        for(int i=0; i<WS2812B_ANZAHL_LEDS; i++) {
            ws2812b_leds[i] = CHSV((hsv+i*5<=255) ? hsv+i*5 : hsv+i*5-255, 255, 255);
        }
        delay(50);
    }
    else {
        FastLED.clear();
    }
    FastLED.show();
    motors4SR.applySpeeds();
    triggerUsSensors();

    lcd.setCursor(0, 0);
    lcd.print("PosX: "+String(fieldPosX));
    lcd.print("__");
    lcd.print("PosY: "+String(fieldPosY));

    readLineDirection();
}

void readLineDirection() {
  Wire.requestFrom(8, 1);

  while(Wire.available()) {
    detectedLine = Wire.read();
  }
}

void playBuzzerFeedbackSound() {
    pinMode(BUZZER_PIN, OUTPUT);
    digitalWrite(BUZZER_PIN, HIGH);                             //enable buzzer
    delay(100);
    digitalWrite(BUZZER_PIN, LOW);
}

void triggerUsSensors() {
  /*
   * function used to trigger all the HC-SR04 readouts. It got a bit complicated, since I have chosen to only allow one sensor readout at a time to prevent any interference between the
   * different sensors. This could otherwise cause problems, since there are all working on the same frequency and in the soccerfield the sound can by reflected quite a lot. Therefore
   * I've written this function which is taking care of that problem, while still making sure that we get quite a lot of sensor readouts. (As much as possible)
   */
  boolean tooLongTimeHasPassed = ((micros() - currentUsSensorTimeTriggered) > 36250);
  if (tooLongTimeHasPassed && gotUsSReadout[currentUsSensor] == false) {
    ultrasonicDistanceValuesMM[currentUsSensor]=-1;                                      //if we didn't get a readout we will set the value to -1 to not get stuck with old readouts
  }
  /*
   * if the last triggered ultrasonic sensor has already returned its distance value we can already initiate a readout of the next sensor
   * if we didn't get a response for 18100us or 0.0181s we will probably get no data from that readout and we can skip to the next sensor. The sensor is either disconnected, broken or the object/wall is just out of range.
   * The 18100us derive from a datasheet where they have mentioned the max distance of 300cm where the sound would need 18100 us at 0°C to travel that distance. https://www.mikrocontroller.net/attachment/218122/HC-SR04_ultraschallmodul_beschreibung_3.pdf
   * To be precise 18100us are still too short, since the time has to travel that distance two times and the readout has to be given to the Arduino. But currently I think it is a good tradeof for performance.
   */
  if (gotUsSReadout[currentUsSensor] || tooLongTimeHasPassed) {                           //regardless of whether we got a sensor readout or not for that given time, we can initialise the next readout
    if (currentUsSensor < 3) {                                                            //if we have not already reached the last of my ultrasonic sensors and therefore reached the last trigger pin
      currentUsSensor++;                                                                  //change the currentUsSensor to the next successive sensor
    }
    else {                                                                                //if we have already reached the last sensor
      currentUsSensor=0;                                                                  //start again with the first ultrasonic sensor
    }
    gotUsSReadout[currentUsSensor] = false;                                               //set gotUsSReadout[currentUsSensor] to false to be later be able to tell if we got a readout
    int trigpin;
    switch(currentUsSensor) {
        case 0:
            trigpin=10;
            break;
        case 1:
            trigpin=12;
            break;
        case 2:
            trigpin=13;
            break;
        case 3:
            trigpin=11;
            break;
    }
    digitalWrite(trigpin, LOW);                               //setting the trigger pin at first to low for 3 us
    delayMicroseconds(3);
    digitalWrite(trigpin, HIGH);                              //set trigger pin to HIGH for 10 us
    delayMicroseconds(10);
    digitalWrite(trigpin, LOW);                               //set trigger pin back to LOW
    currentUsSensorTimeTriggered = micros();                  //save the current micros time to be able to tell later how long ago that was
  }
}

ISR(PCINT1_vect) {
    unsigned long current_time = micros();
    //UsS2=========================================
  if(PINJ & B00000010){                                        //Is input 14 high?
    if(stateUsS2 == 0){                                        //Input 14 changed from 0 to 1
      stateUsS2 = 1;                                           //Remember current input state
      timerUsS2 = current_time;                                //Set timerUsS2 to current_time
    }
  }
  else if(stateUsS2 == 1){                                     //Input 14 is not high and changed from 1 to 0
    stateUsS2 = 0;                                             //Remember current input state
    rawInputUsS[2] = current_time - timerUsS2;                 //rawInputUsS2 is current_time - timerUsS2
    gotUsSReadout[2] = true;
    ultrasonicDistanceValuesMM[2]=rawInputUsS[2]/(2*2.91f);   //when we got a readout, we can convert this us time value to a distance value in mm (derives from speed of sound) and store it in the array
  }
  //UsS3=========================================
  if(PINJ & B00000001){                                        //Is input 15 high?
    if(stateUsS3 == 0){                                        //Input 15 changed from 0 to 1
      stateUsS3 = 1;                                           //Remember current input state
      timerUsS3 = current_time;                                //Set timerUsS3 to current_time
    }
  }
  else if(stateUsS3 == 1){                                     //Input 15 is not high and changed from 1 to 0
    stateUsS3 = 0;                                             //Remember current input state
    rawInputUsS[3] = current_time - timerUsS3;                 //rawInputUsS3 is current_time - timerUsS3
    gotUsSReadout[3] = true;
    ultrasonicDistanceValuesMM[3]=rawInputUsS[3]/(2*2.91f);   //when we got a readout, we can convert this us time value to a distance value in mm (derives from speed of sound) and store it in the array
  }
}

ISR(PCINT2_vect){
  unsigned long current_time = micros();
  //UsS0=========================================
  if(PINK & B00000100){                                        //Is input A10 high?
    if(stateUsS0 == 0){                                        //Input A10 changed from 0 to 1
      stateUsS0 = 1;                                           //Remember current input state
      timerUsS0 = current_time;                                //Set timerUsS0 to current_time
    }
  }
  else if(stateUsS0 == 1){                                     //Input A10 is not high and changed from 1 to 0
    stateUsS0 = 0;                                             //Remember current input state
    rawInputUsS[0] = current_time - timerUsS0;                 //rawInputUsS0 is current_time - timerUsS0
    gotUsSReadout[0] = true;
    ultrasonicDistanceValuesMM[0]=rawInputUsS[0]/(2*2.91f);    //when we got a readout, we can convert this us time value to a distance value in mm (derives from speed of sound) and store it in the array
  }
  //UsS1=========================================
  if(PINK & B00000010){                                        //Is input A9 high?
    if(stateUsS1 == 0){                                        //Input A9 changed from 0 to 1
      stateUsS1 = 1;                                           //Remember current input state
      timerUsS1 = current_time;                                //Set timerUsS1 to current_time
    }
  }
  else if(stateUsS1 == 1){                                     //Input A9 is not high and changed from 1 to 0
    stateUsS1 = 0;                                             //Remember current input state
    rawInputUsS[1] = current_time - timerUsS1;                 //rawInputUsS1 is current_time - timerUsS1
    gotUsSReadout[1] = true;
    ultrasonicDistanceValuesMM[1]=rawInputUsS[1]/(2*2.91f);   //when we got a readout, we can convert this us time value to a distance value in mm (derives from speed of sound) and store it in the array
  }
}