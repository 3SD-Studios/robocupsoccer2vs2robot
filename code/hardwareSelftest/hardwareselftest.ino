/* Copyright Tobias Veselsky - All Rights Reserved
 * Unauthorized copying of this file or any file contained in this repository, via any medium is strictly prohibited
 * Proprietary and confidential
 * Usage of this or any file in this repository has to be authored by Tobias Veselsky (3sd.studios@gmail.com)
 * Copyright to any authorized derivative of this project still belongs to Tobias Veselsky (3SD-Studios)
 * Unauthorized derivatives are prohibited
 * Written by Tobias Veselsky (3SD-Studios) <3sd.studios@gmail.com>, published May 2020
 */


#include <Adafruit_BNO055.h>
#include "FastLED.h"

/**
 * motor-pinmapping: IN1 and IN2 are 2 inputs pins of the respective L6205N's.
 * The direction of rotation can be specified via these 2 pins.
 * By setting both to true or both to false, the motor stops spinning.
 * The SPEED pin is used to set the motor speed via pulse width modulation.
 **/
#define MOTOR1_IN1 40
#define MOTOR1_IN2 38
#define MOTOR1_SPEED 5
#define MOTOR2_IN1 44
#define MOTOR2_IN2 42
#define MOTOR2_SPEED 4
#define MOTOR3_IN1 46
#define MOTOR3_IN2 48
#define MOTOR3_SPEED 3
#define MOTOR4_IN1 47
#define MOTOR4_IN2 49
#define MOTOR4_SPEED 2

/* pushbutton-pinmapping */
#define BUTTON_1 41
#define BUTTON_2 39
#define BUTTON_3 28
#define BUTTON_4 29

/* poti-pinmapping */
#define POTI_1 A11
#define POTI_2 A12

/* led-pinmapping */
#define LED_1 24
#define LED_2 23
#define LED_3 25
#define LED_4 22

/* light-barrier-pinmapping */
#define LBARRIER_TX 37
#define LBARRIER_RX A13

/* buzzer */
#define BUZZER_PIN 31

/* kicker */
#define KICKER_PIN 26

/* WS2812B */
#define WS2812B_ANZAHL_LEDS 32
#define WS2812B_PIN 45

Adafruit_BNO055 bno055 = Adafruit_BNO055(55);

CRGB ws2812b_leds[WS2812B_ANZAHL_LEDS];

void setup()
{
    Serial.begin(9600);                                         //Establish a Serial Connection with a Baud rate of 9600
    while(!Serial) {}
    Serial.println("Starting Hardware-Selftest\n");

    FastLED.addLeds<NEOPIXEL, WS2812B_PIN>(ws2812b_leds, WS2812B_ANZAHL_LEDS);

    if(!bno055.begin())
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("No BNO055 detected ... Check your wiring or I2C ADDR!");
        while(1);
    }
    bno055.setExtCrystalUse(true);                              //it is recommended to use the external crystal for better accuracy

    boolean motorTest = testAllMotors();                        //Every test that ran succesfully will return true
    boolean pushbuttonTest = testAllButtons();                  //pushbuttonTest returns true if every pushbutton is working
    boolean potiTest = testAllPotis();                          //run function to test all the potentiometers. The functions returns true if all potis are working
    boolean ledTest = testAllLEDs();                            //test every single LED if it is working
    boolean lightBarrierTest = testLightBarrier();              //test the function of the lightbarrier
    boolean compasstestresult = compassTest();                  //test whether the compass is working
    displayTestResults(motorTest, pushbuttonTest, potiTest, ledTest, lightBarrierTest, compasstestresult);
    kickerTest();                                               //trigger kicker once
    ws2812bTest();
    playBuzzerFeedbackSound();
}

void loop()
{
    //simply do nothing, the test is supposed to run only one time
}

void ws2812bTest() {
    for(int i = 0; i < WS2812B_ANZAHL_LEDS; i++) {
        ws2812b_leds[i] = CRGB::Blue;
        FastLED.show();
        ws2812b_leds[i] = CRGB::Black;
        delay(100);
    }
}

void kickerTest() {
    pinMode(KICKER_PIN, OUTPUT);
    digitalWrite(KICKER_PIN, HIGH);
    delay(50);
    digitalWrite(KICKER_PIN, LOW);
}

void playBuzzerFeedbackSound() {
    pinMode(BUZZER_PIN, OUTPUT);
    digitalWrite(BUZZER_PIN, HIGH);                             //enable buzzer
    delay(100);
    digitalWrite(BUZZER_PIN, LOW);
}

boolean compassTest() {
    Serial.println("Testing the BNO055 compass/orientation sensor");

    /* Get a new sensor event */ 
    sensors_event_t event; 
    bno055.getEvent(&event);
  
    /* Display the floating point data */
    Serial.print("X: ");
    Serial.print(event.orientation.x, 4);
    Serial.print("\tY: ");
    Serial.print(event.orientation.y, 4);
    Serial.print("\tZ: ");
    Serial.print(event.orientation.z, 4);
    Serial.println("");
    return event.orientation.x != 0;
}

boolean testLightBarrier() {
    Serial.println("Testing the LED lightbarrier");

    pinMode(LBARRIER_TX, OUTPUT);
    pinMode(LBARRIER_RX, INPUT);

    digitalWrite(LBARRIER_TX, HIGH);
    Serial.println("Please take the ball out of the robot and confirm that by sending \"y\".");
    if (!serialYesOrNo()) return false;
    int valueWithoutBall = analogRead(LBARRIER_RX);
    Serial.println("Value without the ball: " + String(valueWithoutBall));
    Serial.println("Please put the ball into the lightbarrier and confirm that by sending \"y\"");
    if (!serialYesOrNo()) return false;
    int valueWithBall = analogRead(LBARRIER_RX);
    Serial.println("Value with the ball: " + String(valueWithBall));
    Serial.println("___________________________________________________________________________");
    return valueWithoutBall>valueWithBall;
}

boolean testAllLEDs() {
    Serial.println("Testing all the LEDs:");
    
    pinMode(LED_1, OUTPUT);                                     //set LED 1 pin as OUTPUT
    pinMode(LED_2, OUTPUT);                                     //set LED 2 pin as OUTPUT
    pinMode(LED_3, OUTPUT);                                     //set LED 3 pin as OUTPUT
    pinMode(LED_4, OUTPUT);                                     //set LED 4 pin as OUTPUT

    Serial.println("Is any LED currently turned ON? Enter \"y\" if one is turned on. Otherwise enter \"n\".");
    if (serialYesOrNo()) return false;                          //if one LED is lit up, eventhough none is turned on, there is probably a pcb issue or something else.

    Serial.println("Testing LED nr. 1:");
    boolean resultLED1 = ledTest(LED_1);                        //test LED 1
    Serial.println("Testing LED nr. 2:");
    boolean resultLED2 = ledTest(LED_2);
    Serial.println("Testing LED nr. 3:");
    boolean resultLED3 = ledTest(LED_3);
    Serial.println("Testing LED nr. 4:");
    boolean resultLED4 = ledTest(LED_4);

    Serial.println("___________________________________________________________________________");
    return resultLED1 && resultLED2 && resultLED3 && resultLED4;    //return true if every LED is working
}

boolean ledTest(int pin) {
    digitalWrite(pin, HIGH);
    Serial.println("Is the LED now turned ON? Enter Enter \"y\" if it is turned on. Otherwise enter \"n\".");
    boolean result = serialYesOrNo();
    return result;
}

boolean serialYesOrNo() {
    while(Serial.available() <= 0);
    while (true) {
        if (Serial.available() > 0) {
            char character = Serial.read();
            if (character == 'y') {
                return true;
            }
            else if (character == 'n') {
                return false;
            }
            else {
                Serial.println("Please enter only the allowed characters!");
                Serial.flush();
            }
        }
    }
}

boolean testAllPotis() {
    /**
     * Function which promts the user to rotate the potentiometer knob and tests if they are working as supposed to.
     * Funtion returns true if all potis are working correctly.
     **/
    Serial.println("Testing all the potentiometers:");
    pinMode(POTI_1, INPUT);                                     //set poti 1 pin as input
    pinMode(POTI_2, INPUT);                                     //set poti 2 pin as input

    Serial.println("Testing potentiometer nr.1:");
    boolean resultPoti1 = potiTest(POTI_1);
    Serial.println("Testing potentiometer nr.2:");
    boolean resultPoti2 = potiTest(POTI_2);
    Serial.println("___________________________________________________________________________");
    return resultPoti1 && resultPoti2;    //return true if every potentiometer is working
}

boolean potiTest(int pin) {
    /**
     * In this function, the poti is tested by promting the user to set the potentiometer to different positions
     * and then checking in the code if they are reached.
     * This function returns only true if every value was reached as supposed.
     **/
    if (abs(analogRead(pin) - 500) > 25) Serial.println("Please set the poti knob position to 50%!");
    if (!checkpotiposition(pin, 512)) return false;
    Serial.println("Please set the poti position to 0%!");
    if (!checkpotiposition(pin, 0)) return false;
    Serial.println("Please set the poti position now to 100%!");
    if (!checkpotiposition(pin, 1024)) return false;
    Serial.println("Please return the poti position now back to 50%!");
    if (!checkpotiposition(pin, 512)) return false;
    Serial.println("_________________________");
    return true;
}

boolean checkpotiposition(int pin, int referenceposition) {
    /**
     * int pin is the pin of the poti and referenceposition is the analogValue that the poti is supposed to "have".
     * If the value is not in the given range of +-25 to the referenceposition, the function returns false and the potentiometer is probably broken.
     **/
    long timer = micros();
    while (abs(analogRead(pin) - referenceposition) > 25) {
        if (micros() - timer > 10000000) {                                              //if more than 10 seconds passed
            Serial.println("Timeout! The potentiometer seems to be broken!");
            return false;
        }
    }
    return true;
}

boolean testAllButtons() {
    /**
     * Function which promts the user to press all the different buttons and checks whether all the button are working.
     * The function returns true if all the buttons are working as they are supposed to
     **/
    Serial.println("Testing all the push-buttons:");
    initialisePushButton(BUTTON_1);                             //initialising every Pushbutton
    initialisePushButton(BUTTON_2);
    initialisePushButton(BUTTON_3);
    initialisePushButton(BUTTON_4);

    Serial.println("Testing pushbutton nr. 1:");
    boolean resultButton1 = pushbuttonTest(BUTTON_1);           //starts the pushbutton test which returns true if the pushbutton is working
    Serial.println("Testing pushbutton nr. 2:");
    boolean resultButton2 = pushbuttonTest(BUTTON_2);
    Serial.println("Testing pushbutton nr. 3:");
    boolean resultButton3 = pushbuttonTest(BUTTON_3);
    Serial.println("Testing pushbutton nr. 4:");
    boolean resultButton4 = pushbuttonTest(BUTTON_4);
    Serial.println("___________________________________________________________________________");
    return resultButton1 && resultButton2 && resultButton3 && resultButton4;    //return true if every pushbutton is working
}

boolean pushbuttonTest(int pin) {
    if (digitalRead(pin) == HIGH) return false;                                 //if the Arduino already detects the pushbutton as pressed even before the user is supposed to press the button, most likely the button is stuck/broken
    Serial.println("Please press the pushbutton!");
    long timer = micros();
    while (digitalRead(pin) == LOW) {
        if (micros() - timer > 5000000) {                                       //if the button is not pressed for at least 5 secs, declare the button as broken
            Serial.println("Timeout! The Button seems to be broken!");
            return false;
        }
    }
    Serial.println("Button was pressed. Button seems to be okay!");
    Serial.println("_________________________");
    return true;                                                                //if the function is not exited earlier, the button seems to be okay
}

void initialisePushButton(int pin) {
    pinMode(pin, INPUT);                                                        //just set the pin as input (probably unnessary, because the pinMode is set as an input by default)
}

boolean testAllMotors() {
    /**
     * Function which can be called to initialise and test all 4 motors
     **/
    Serial.println("Testing all 4 Motors in both directions in different speeds. Please check whether the motors are working or not!");

    initialiseMotor(MOTOR1_IN1, MOTOR1_IN2, MOTOR1_SPEED);
    initialiseMotor(MOTOR2_IN1, MOTOR2_IN2, MOTOR2_SPEED);
    initialiseMotor(MOTOR3_IN1, MOTOR3_IN2, MOTOR3_SPEED);
    initialiseMotor(MOTOR4_IN1, MOTOR4_IN2, MOTOR4_SPEED);

    Serial.println("Testing Motor1:");
    motorTest(MOTOR1_IN1, MOTOR1_IN2, MOTOR1_SPEED);
    Serial.println("_________________________");
    Serial.println("Testing Motor2:");
    motorTest(MOTOR2_IN1, MOTOR2_IN2, MOTOR2_SPEED);
    Serial.println("_________________________");
    Serial.println("Testing Motor3:");
    motorTest(MOTOR3_IN1, MOTOR3_IN2, MOTOR3_SPEED);
    Serial.println("_________________________");
    Serial.println("Testing Motor4:");
    motorTest(MOTOR4_IN1, MOTOR4_IN2, MOTOR4_SPEED);
    Serial.println("_________________________");

    Serial.println("MotorSelftest ran succesfully without any issues. :)");
    Serial.println("___________________________________________________________________________");
    return true;                                                //return true if the test did not ran into any issues. Since the functionality of the motors has to be controlled by the user, this function usually returns true.
}

void initialiseMotor(int motor_IN1, int motor_IN2, int motor_SPEED) {
    /**
     * function to set the pinMode of the respective motor
     **/
    pinMode(motor_IN1, OUTPUT);
    pinMode(motor_IN2, OUTPUT);
    pinMode(motor_SPEED, OUTPUT);
}

void motorTest(int motor_IN1, int motor_IN2, int motor_SPEED) {
    /**
     * Function to spin up the respective motor in both directions with different speeds and abruptly stop the motor in the end.
     **/
    Serial.println("Direction 1");
    digitalWrite(motor_IN1, LOW);
    digitalWrite(motor_IN2, HIGH);
    for (int i=0; i<=255; i+=25) {
        analogWrite(motor_SPEED, i);
        Serial.println("speed: " + String(i));
        delay(300);
    }
    Serial.println("Direction 2");
    digitalWrite(motor_IN1, HIGH);
    digitalWrite(motor_IN2, LOW);
    for (int i=0; i<=255; i+=25) {
        analogWrite(motor_SPEED, i);
        Serial.println("speed: " + String(i));
        delay(300);
    }
    Serial.println("MotorForceStop");
    digitalWrite(motor_IN1, LOW);
    digitalWrite(motor_IN2, LOW);
    delay(500);
}

void displayTestResults(boolean motorTest, boolean pushbuttonTest, boolean potiTest, boolean ledTest, boolean lightBarrierTest, boolean compassTestResult) {
    Serial.println("Hardware-Selftest Results:");
    Serial.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
    String motorTitle = "Motor-Test";
    String pushbuttonTitle = "Pushbutton-Test";
    String potiTitle = "Potentiometer-Test";
    String LEDTitle = "LED-Test";
    String lightbarrierTitle = "Lightbarrier-Test";
    String compassTestTitle = "BNO055-Compass-Test";
    while (motorTitle.length() < 19)                                            //add spaces to the title to align the results
    {
        motorTitle+=" ";
    }
    motorTitle+= (motorTest) ? ": Passed" : ": An error occured. Broken?";      //add the result to the String
    Serial.println(motorTitle);                                                 //print the whole String

    while (pushbuttonTitle.length() < 19)
    {
        pushbuttonTitle+=" ";
    }
    pushbuttonTitle+= (pushbuttonTest) ? ": Passed" : ": An error occured. Broken?";
    Serial.println(pushbuttonTitle);
    
    while (potiTitle.length() < 19)
    {
        potiTitle+=" ";
    }
    potiTitle+= (potiTest) ? ": Passed" : ": An error occured. Broken?";
    Serial.println(potiTitle);

    while (LEDTitle.length() < 19)
    {
        LEDTitle+=" ";
    }
    LEDTitle+= (ledTest) ? ": Passed" : ": An error occured. Broken?";
    Serial.println(LEDTitle);

    while (lightbarrierTitle.length() < 19)
    {
        lightbarrierTitle+=" ";
    }
    lightbarrierTitle+= (lightBarrierTest) ? ": Passed" : ": An error occured. Broken?";
    Serial.println(lightbarrierTitle);

    while (compassTestTitle.length() < 19)
    {
        compassTestTitle+=" ";
    }
    compassTestTitle+= (compassTestResult) ? ": Passed" : ": An error occured. Broken?";
    Serial.println(compassTestTitle);

    Serial.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
    boolean result = motorTest && pushbuttonTest && potiTest && ledTest && lightBarrierTest && compassTestResult;               //result of all performed tests
    Serial.println(String("Selftest-Result: ") + ((result) ? "Passed" : "Not Passed! Error occured or defective robot"));       //display the endresult
    Serial.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
}